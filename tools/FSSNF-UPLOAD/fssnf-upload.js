/*
 * Copyright 2019 Diona.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of Diona
 * ("Confidential Information"). You shall not disclose such Confidential Information
 *  and shall use it only in accordance with the terms of the license agreement you
 *  entered into with Diona.
 */

var fs = require('fs');

var AppUtil = require('../../server/framework/server/AppUtil');
var Promise = AppUtil.resolve('./framework/DMPromise').Promise;
var dbstore = AppUtil.resolve('./framework/persistence/dbstore');
var Util = AppUtil.resolve('./framework/util/Util');
var CasesData = require('./model/CasesData');
var ClientsData = require('./model/ClientsData');
var NotesData = require('./model/NotesData');
var CodeLookupsData = require('./model/CodeLookupsData');
var FormsData = require('./model/FormsData');

var COLLECTION_DMSOR_CASES = 'Cases';
var COLLECTION_DMSOR_CLIENTS = 'Clients';

var CASE_ID = 'Case ID';
var CASE_NAME = 'Case Name';
var CASE_TYPE = 'Case Type';
var SERVICE_ROLE = 'Service Role';

var DM_CASE_ROLE = 'DMCaseRole';

var XLSX = require('xlsx'),
  wb;

var dataExtract = process.argv[2];
var sheets = parseWorksheets(dataExtract);

return Promise.each(sheets.casesAndClients, function (basicInformationRow) {
  var caseID = basicInformationRow[CASE_ID].toString();

  return CasesData.readCase(caseID).then(function (caseObj) {

    var username = 'caseworker';
    for (var i = 0; i < sheets.users.length; i++) {
      if (basicInformationRow["Worker ID"].toString() === sheets.users[i]["Worker ID"].toString()) {
        console.log(basicInformationRow[CASE_ID] + 'belongs to' + sheets.users[i].Username);
        username = sheets.users[i].Username;
      }
    }

    var caseDocument = {
      caseID: caseID,
      reference: basicInformationRow[CASE_NAME],
      customData: basicInformationRow[CASE_ID] + " - " + basicInformationRow[CASE_TYPE],
      username: username
    };

    console.log(caseDocument);

    return CasesData.getCaseAddress(caseID, sheets.caseAddress).then(function (addressStr) {

      if (addressStr) {
        caseDocument.address = addressStr;
      }

      // If case exists update it, otherwise create it.
      if (caseObj) {
        //console.log("Case " + caseID + " already exists.");
        return CasesData.editCase(caseDocument);
      } else {
        //console.log("Case " + caseID + " does not exist.");
        caseDocument.members = [];
        return CasesData.newCase(caseDocument);
      }
    }).then(ClientsData.createOrUpdateClient(basicInformationRow, sheets.participantAddresses)
      .then(function (clientID) {
        return CodeLookupsData.addOrUpdateCodeLookup({
          name: DM_CASE_ROLE,
          value: basicInformationRow["Service Role"]
        }).then(function (code) {
          return CasesData.addOrUpdateCaseMember({
            clientID: clientID,
            caseID: caseID,
            role: code
          });
        });
      }));
  });
}).then(function () {
  return NotesData.addNotes(sheets.notes, sheets.noteParticipants);
}).then(function () {
  console.log("Done");
  process.exit();
});

function parseWorksheets(dataExtract) {
  var sheets = {};
  sheets.participantAddresses = parseWorksheet(
  dataExtract + '/SFTP_04FSSN_Diona_Participant_Addresses.xlsx');
  sheets.casesAndClients = parseWorksheet(
    dataExtract + '/SFTP_04FSSN_Diona_Basic_Information.xlsx');
  sheets.caseAddress = parseWorksheet(
    dataExtract + '/SFTP_04FSSN_Diona_Case_Address.xlsx');
  sheets.notes = parseWorksheet(
    dataExtract + '/SFTP_04FSSN_Diona_Note_Description.xlsx');
  sheets.noteParticipants = parseWorksheet(
    dataExtract + '/SFTP_04FSSN_Diona_Note_Participants.xlsx');
  sheets.users = parseWorksheet(
    dataExtract + '/../Users.xls');
  return sheets;
}

function parseWorksheet(worksheetLocation) {

  var workBook = XLSX.readFile(worksheetLocation, {
    cellDates: true
  });
  var sheet_name_list = workBook.SheetNames;
  var headers = {};
  var data = [];

  //var workSheet = workBook.Sheets['Report 1'];
  var workSheet;
  for (var key in workBook.Sheets) {
    if (workBook.Sheets.hasOwnProperty(key)) {
      workSheet = workBook.Sheets[key];
      break;
    }
  }

  for (var i in workSheet) {
    if (i[0] === '!') continue;

    //parse  the column, row, and value
    var col = i.replace(/[0-9]/g, '');
    var row = parseInt(i.replace(/[A-Z]/g, ''));
    var value = workSheet[i].v;
    //store header names
    if (row == 1) {
      headers[col] = value;
      continue;
    }
    if (!data[row]) {
      data[row] = {};
    }

    data[row][headers[col]] = value;
  }
  //droping the first two rows of empty
  data.shift();
  data.shift();
  return data;
}