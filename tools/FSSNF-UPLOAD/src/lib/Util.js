/*
 * Copyright 2019 Diona.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of Diona
 * ("Confidential Information"). You shall not disclose such Confidential Information
 *  and shall use it only in accordance with the terms of the license agreement you
 *  entered into with Diona.
 */
var fs = require('fs');
var path = require('path');

var sortOrderArr = [{
    a: -1,
    b: 1
}, {
    a: 1,
    b: -1
}];

function sortByProperty(propertyName, sortOrder) {
    return function (object1, object2) {
        var sortStatus = 0;
        if (object1[propertyName] > object2[propertyName]) {
            sortStatus = sortOrderArr[sortOrder].a;
        } else if (object1[propertyName] < object2[propertyName]) {
            sortStatus = sortOrderArr[sortOrder].b;
        }
        return sortStatus;
    };
};

function findFilesByName(name, dir) {
    var result = [];

    function recursive(dirPath) {
        var fileNames = fs.readdirSync(dirPath);
        for (var i = 0; i < fileNames.length; i++) {
            var fileName = fileNames[i];
            var filePath = path.join(dirPath + "/" + fileName);
            var isDirectory = fs.statSync(filePath).isDirectory();
            // Exclude test folders
            if (isDirectory && fileName != "test") {
                recursive(filePath);
            } else {
                if (fileName == name) {
                    result.push(filePath);
                }
            }
        }
    }
    recursive(dir);
    return result;
}

function loadFilesAsArray(name, dir) {
    var result = [];
    var files = findFilesByName(name, dir);
    files.forEach(filePath => {
        var contentsArray = require(filePath);
        contentsArray.forEach(item => {
            result.push(item);
        });
    });
    return result;
}

exports.sortByProperty = sortByProperty;
exports.findFilesByName = findFilesByName;
exports.loadFilesAsArray = loadFilesAsArray;