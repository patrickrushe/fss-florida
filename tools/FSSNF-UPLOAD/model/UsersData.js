/*
 * Copyright 2019 Diona.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of Diona
 * ("Confidential Information"). You shall not disclose such Confidential Information
 *  and shall use it only in accordance with the terms of the license agreement you
 *  entered into with Diona.
 */

var AppUtil = require('../../../server/framework/server/AppUtil');
var dbstore = AppUtil.resolve("./framework/persistence/dbstore");
var DMSecurity = AppUtil.resolve("./framework/DMSecurity");
var Promise = require("bluebird");

var USERS_COLLECTION = "Users";

function listUsers() {
    return dbstore.readMulti_p(USERS_COLLECTION, {}, {
        _id: 0,
        password: 0
    }, {
        username: 1
    }).then(users => {
        users.forEach(user => {
            if (user.role) {
                switch (user.role) {
                case 'ADMIN':
                    user.roleStr = "Administrator";
                    break;
                case 'INTAKE':
                    user.roleStr = "Intake";
                    break;
                case 'SUPERVISOR':
                    user.roleStr = "Supervisor";
                    break;
                case 'CASEWORKER':
                    user.roleStr = "Case Worker";
                    break;
                case 'INVESTIGATOR':
                    user.roleStr = "Investigator";
                    break;
                default:
                    break;
                }
            }
            // Set the status display value
            user.statusStr = (user.status === "ACTIVE") ? "Active" : "Deactivated";
        });
        return users;
    });
}

function insertUser(user) {
    return DMSecurity.generateHash({
        password: user.password
    }).then((hash) => {
        user.password = hash;
        user.status = "ACTIVE";
        return dbstore.insertOne_p(USERS_COLLECTION, user);
    });
}

function editUser(user) {
    var updateSpec = {
        firstname: user.firstname,
        lastname: user.lastname,
        role: user.role
    };
    return dbstore.updateExactlyOne_p(USERS_COLLECTION, {
        username: user.username
    }, {
        $set: updateSpec
    });
}

function deleteUser(username) {
    return dbstore.removeExactlyOne_p(USERS_COLLECTION, {
        username: username
    });
}

function changeUserStatus(user) {
    var updateSpec = {
        status: user.status
    };
    return dbstore.updateExactlyOne_p(USERS_COLLECTION, {
        username: user.username
    }, {
        $set: updateSpec
    });
}

function changePassword(params) {
    return new Promise(function (resolve) {
        // If the current password is provided, check it matches the existing
        if (params.currentPassword) {
            return DMSecurity.generateHash({
                password: params.currentPassword
            }).then((currentPasswordHash) => {
                return dbstore.readExactlyOne_p(USERS_COLLECTION, {
                    username: params.username
                }).then(user => {
                    if (user.password !== currentPasswordHash) {
                        return resolve({
                            success: false,
                            message: "Update password failed. Invalid current password provided."
                        });
                    } else {
                        return resolve({
                            success: true
                        });
                    }
                });
            });
        } else {
            return resolve({
                success: true
            });
        }
    }).then(function (result) {
        if (!result.success) {
            return result;
        } else {
            return DMSecurity.generateHash({
                password: params.newPassword
            }).then((hash) => {
                var updateSpec = {
                    password: hash
                };
                return dbstore.updateExactlyOne_p(USERS_COLLECTION, {
                    username: params.username
                }, {
                    $set: updateSpec
                }).then(() => {
                    return {
                        success: true
                    }
                });
            });
        }
    });
}

exports.listUsers = listUsers;
exports.insertUser = insertUser;
exports.editUser = editUser;
exports.deleteUser = deleteUser;
exports.changePassword = changePassword;
exports.changeUserStatus = changeUserStatus;