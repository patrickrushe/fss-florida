/*
 * Copyright 2019 Diona.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of Diona
 * ("Confidential Information"). You shall not disclose such Confidential Information
 *  and shall use it only in accordance with the terms of the license agreement you
 *  entered into with Diona.
 */

var AppUtil = require('../../../server/framework/server/AppUtil');
var dbstore = AppUtil.resolve("./framework/persistence/dbstore");
var configuration = AppUtil.resolve("./framework/configuration/configuration");
var Promise = require("bluebird");

var SYSTEM_PROPERTIES_COLLECTION = "SystemProperties";

function listSettings() {
    return dbstore.readMulti_p(SYSTEM_PROPERTIES_COLLECTION, {
        appID: "DMDA"
    }, {
        _id: 0,
        appID: 0
    }, {
        name: 1
    }).then(systemProperties => {
        var settings = [];
        systemProperties.forEach(systemProperty => {
            var metadata = systemProperty.metadata;
            if (typeof metadata == "array") {
                // TODO - Add support for nested properties if we ever need it
            } else {
                var setting = {
                    name: systemProperty.name,
                    category: systemProperty.category,
                    label: metadata.label,
                    description: metadata.description,
                    type: metadata.type,
                    value: systemProperty.value
                };
                settings.push(setting);
            }
        });
        return settings;
    });
}

function updateSetting(setting) {
    var updateSpec = {
        value: setting.value
    };
    return dbstore.updateExactlyOne_p(SYSTEM_PROPERTIES_COLLECTION, {
        name: setting.name
    }, {
        $set: updateSpec
    });
}

function getSettings() {
    return listSettings().then(settings => {
        return configuration.formatPropertiesArray(settings);
    });
}

exports.listSettings = listSettings;
exports.updateSetting = updateSetting;
exports.getSettings = getSettings;