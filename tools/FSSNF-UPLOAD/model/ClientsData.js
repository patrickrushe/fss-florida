/*
 * Copyright 2019 Diona.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of Diona
 * ("Confidential Information"). You shall not disclose such Confidential Information
 *  and shall use it only in accordance with the terms of the license agreement you
 *  entered into with Diona.
 */

var AppUtil = require('../../../server/framework/server/AppUtil');
var dbstore = AppUtil.resolve("./framework/persistence/dbstore");
var CodeLookupsData = require('./CodeLookupsData');
var Promise = require("bluebird");


var CLIENTS_COLLECTION = "Clients";

function createOrUpdateClient(params, clientAddresses) {

  var clientDocument = {
    clientID: params["Case Participant ID"].toString(),
    firstName: params["Case Participant Name"].substr(params["Case Participant Name"].indexOf(
      ',') + 2, params["Case Participant Name"].length - 1),
    lastName: params["Case Participant Name"].substr(0, params["Case Participant Name"].indexOf(
      ',')),
    gender: params.Gender == "Male" ? "M" : "F",
    customData: [{
      fieldID: "reference",
      value: params["Case Participant ID"].toString()
    }],
    readOnly: true
  };


  if (params["Child Date of Birth"]) {
    clientDocument.dateOfBirth = params["Child Date of Birth"].toISOString().slice(0, 10);
  }

  return getClientContactInformation(params["Case Participant ID"].toString(), clientAddresses,
    clientDocument).then(function(clientDocument) {

    return dbstore.upsertExactlyOne_p(CLIENTS_COLLECTION, {
      clientID: params["Case Participant ID"].toString()
    }, {
      $set: clientDocument
    });
  }).then(() => {
    return clientDocument.clientID;
  });
}

function getClientContactInformation(clientID, caseAddresses, clientDocument) {
  var addressStr = "";

  var i = 0;
  return Promise.each(caseAddresses, function(address) {
    i++;
    var addressStr = "";

    if (address["Person Id"].toString() === clientID && !address["Effective End Date"]) {

      if (address.Phone || address["Address Cell Phone"]) {
        if (!clientDocument.phoneNumbers) {
          clientDocument.phoneNumbers = [];
        }

        if (address.Phone) {
          clientDocument.phoneNumbers.push({
            phoneID: clientID + ("000" + i).slice(-4) + 'P1',
            number: address.Phone,
            phoneType: "HOME"
          });

          if (address["Address Type"] === "Primary Residence") {
            clientDocument.customData.push({
              fieldID: "primaryHomePhoneNumber",
              value: address.Phone
            })
          }
        }

        if (address["Address Cell Phone"]) {
          clientDocument.phoneNumbers.push({
            phoneID: clientID + ("000" + i).slice(-4) + 'C1',
            number: address["Address Cell Phone"],
            phoneType: "MOBILE"
          });
          if (address["Address Type"] === "Primary Residence") {
            clientDocument.customData.push({
              fieldID: "primaryCellPhoneNumber",
              value: address["Address Cell Phone"]
            })
          }
        }
      }

      if (address.Email) {
        clientDocument.email = address.Email;
      }

      if (address["Street Number"]) {
        addressStr = address["Street Number"];
      }

      if (address["Street Name"]) {
        addressStr = addressStr + " " + address["Street Name"];
      }

      if (address.Town) {
        addressStr = addressStr + ", " + address.Town;
      }

      if (address.County) {
        addressStr = addressStr + ", " + address.County;
      }

      if (addressStr != "") {
        if (address["State Code"]) {
          addressStr = addressStr + ", " + address["State Code"];
        }

        if (address["Zip Code"]) {
          if (address["Zip Code"].length > 5) {
            addressStr = addressStr + " " + address["Zip Code"].substring(0, 5);
          } else {
            addressStr = addressStr + " " + address["Zip Code"];
          }
        }

        addressStr = addressStr + ",  United States";

        if (address["Address Type"] === "Primary Residence") {
          clientDocument.customData.push({
            fieldID: "primaryAddress",
            value: addressStr
          })
        }

        if (!clientDocument.addressList) {
          clientDocument.addressList = [];
        }

        return CodeLookupsData.addOrUpdateCodeLookup({
          name: "DMAddressType",
          value: address["Address Type"]
        }).then(function(code) {
          clientDocument.addressList.push({
            addressID: clientID + ("000" + i).slice(-4),
            addressType: code,
            location: {
              address: addressStr
            }
          });
        });
      }
    }
    return Promise.resolve();
  }).then(function() {
    return Promise.resolve(clientDocument);
  });
}

exports.createOrUpdateClient = createOrUpdateClient;