/*
 * Copyright 2019 Diona.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of Diona
 * ("Confidential Information"). You shall not disclose such Confidential Information
 *  and shall use it only in accordance with the terms of the license agreement you
 *  entered into with Diona.
 */

var AppUtil = require('../../../server/framework/server/AppUtil');
var dbstore = AppUtil.resolve("./framework/persistence/dbstore");

var CODELOOKUP_COLLECTION = "CodeLookups";

var lookupCache;

function addOrUpdateCodeLookup(params) {

    var code = 'UnknownSOR';

    if (params.value) {
        code = params.value.replace(/\s/g, '');
    }

    return dbstore.readAtMostOne_p(CODELOOKUP_COLLECTION, {
        name: params.name
    }).then(function(codeLookup) {
        for (var i = 0; i < codeLookup.values.length; i++) {
            if (codeLookup.values[i].code === code) {
                return code;
            }
        }

        return code;
    });
}

exports.addOrUpdateCodeLookup = addOrUpdateCodeLookup;