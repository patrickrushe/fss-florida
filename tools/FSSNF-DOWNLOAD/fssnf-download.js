/*
 * Copyright 2019 Diona.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of Diona
 * ("Confidential Information"). You shall not disclose such Confidential Information
 *  and shall use it only in accordance with the terms of the license agreement you
 *  entered into with Diona.
 */

var fs = require('fs');

var AppUtil = require('../../server/framework/server/AppUtil');
var Promise = AppUtil.resolve('./framework/DMPromise').Promise;
var dbstore = AppUtil.resolve('./framework/persistence/dbstore');
var Util = AppUtil.resolve('./framework/util/Util');
var CasesData = require('./model/CasesData');
var ClientsData = require('./model/ClientsData');
var NotesData = require('./model/NotesData');
var CodeLookupsData = require('./model/CodeLookupsData');
var DocumentQueue = require('./model/DocumentQueue');
var FormsData = require('./model/FormsData');
var libxmljs = require('libxmljs');

var XLSX = require('xlsx'),
  wb;

const Json2csvParser = require('json2csv').Parser;

Date.prototype.addHours= function(h){
  this.setHours(this.getHours()+h);
  return this;
}

//var dataExtract = process.argv[2];
var dataExtract = 'c:/dimas2/tools/FSSNF-DOWNLOAD/dataextract';

var sheets = parseWorksheets(dataExtract);

if (!fs.existsSync(dataExtract)) {
  fs.mkdirSync(dataExtract);
}

return DocumentQueue.findAll().then(function (pdfDocuments) {

  
  
    const noteFields = ['caseID', 'fsfnLogin', 'noteType', 'noteCategory', 'contactBeginDate', 'contactEndDate', 'noteText', 'workerActivityCode', 'fsfnParticipantID', 'latitude', 'longitude', 'altitude', 'deviceType', 'gpsType', 'gpsAccuracy', 'rdcTypeOfWork'];
    const noteOpts = {
      noteFields,
      delimiter: '|',
      header: 'false',
      quote: ''
    };

    var noteInfo = {
      'caseID': '','fsfnLogin': '','noteType': '','noteCategory': '','contactBeginDate': '','contactEndDate': '','noteText': '','workerActivityCode': '','fsfnParticipantID': '','latitude': '','longitude': '','altitude': '','deviceType': '','gpsType': '','gpsAccuracy': '','rdcTypeOfWork': ''};


    const contactFields = ['caseID', 'fsfnLogin', 'noteType', 'contactBeginDate', 'fsfnParticipantID', 'contactName', 'faceToFaceContact', 'reasonNotSeen', 'contactBeginDate'];
    const contactOpts = {
      contactFields,
      delimiter: '|',
      header: 'false',
      quote: ''
    };

    var index = 0;

    pdfDocuments.forEach(function (form) {
      var xmlDoc = libxmljs.parseXmlString(form.formData);

      if (xmlDoc.get('//HomeVisit-ChildOutOfHome')) {
        console.log('Process Form');
        index++;
        var directory = dataExtract + '/' + form.docID;

        if (!fs.existsSync(directory)) {
          fs.mkdirSync(directory);
        }

        fs.writeFileSync(directory + '/' + form.pdfFileName, form.pdfDocument, {
          encoding: 'base64'
        });

        var noteInfo = {
          'caseID': '','fsfnLogin': '','noteType': '','noteCategory': '','contactBeginDate': '','contactEndDate': '','noteText': '','workerActivityCode': '','fsfnParticipantID': '','latitude': '','longitude': '','altitude': '','deviceType': '','gpsType': '','gpsAccuracy': '','rdcTypeOfWork': ''};
      
        noteInfo.caseID = form.relatedID;

        
        var participantsStr = xmlDoc.get('//participants').text();

        noteInfo.noteText = xmlDoc.get('//additional_comments').text().replace(/\t/g, '&nbsp;&nbsp;&nbsp;&nbsp;').replace(/\n/g, '\\n');

        noteInfo.fsfnLogin = 'TH10286';
        
        var participants = participantsStr.split(" ");

        /*
        for (var i = 0; i < sheets.users.length; i++) {
          
          if (sheets.users[i]["Email"] && form.username.toLowerCase() === sheets.users[i]["Email"].toString().toLowerCase()) {
            
            noteInfo.fsfnLogin = sheets.users[i]["Login Id"];
          }
        }
        */

        if (!noteInfo.fsfnLogin) {
            noteInfo.fsfnLogin = 'TH10286';
        }

        noteInfo.contactBeginDate = Util.toServerDateTimeFormat(new Date());
        noteInfo.contactEndDate = Util.toServerDateTimeFormat(new Date().addHours(1));
        noteInfo.noteType = 1;
        noteInfo.noteCategory = 1;
        noteInfo.workerActivityCode = 3;

        var participantsJSON = [];
        var index = 0;
        console.log("Participants for note: " + noteInfo.caseID);
        participants.forEach(function (participant) {

            var participantJSON = {
              'caseID':'',
              'fsfnLogin':'',
              'noteType':'',
              'contactBeginDate':'',
              'fsfnParticipantID':'', 
              'contactName':'', 
              'faceToFaceContact':'', 
              'reasonNotSeen':''
            };
            
            participantJSON.caseID = noteInfo.caseID;
            participantJSON.fsfnLogin = noteInfo.fsfnLogin;
            participantJSON.noteType = noteInfo.noteType;
            participantJSON.contactBeginDate = noteInfo.contactBeginDate;
            participantJSON.fsfnParticipantID = participant;
            var nameRef = '//participantName' + (index + 1);
            participantJSON.contactName = xmlDoc.get(nameRef).text();
            participantJSON.faceToFaceContact = 'C';
            participantJSON.reasonNotSeen = '';
            participantJSON.contactBeginDate = noteInfo.contactBeginDate;
            participantsJSON.push(participantJSON);
            console.log(JSON.stringify(participantJSON));
            index++;
        });
        
        try {
          const parser = new Json2csvParser(noteOpts);
          const csv = parser.parse(noteInfo);
          fs.writeFileSync(directory + '/' +'caseNote.csv', csv);

          const parser2 = new Json2csvParser(contactOpts);
          const csv2 = parser2.parse(participantsJSON);
        // console.log(csv);
          fs.writeFileSync(directory + '/' +'caseContact.csv', csv2);
        } catch (err) {
          console.error(err);
        }
        return;
      } else {
        console.log("Safety Plan");
    
      }
    });
}).then(function () {
  console.log("Done");
  process.exit();
});

function parseWorksheets(dataExtract) {
  var sheets = {};
  sheets.users = parseWorksheet(
    dataExtract + '../../../FSSNF-UPLOAD/dataextract/SFTP_04FSSN_Diona_Workers.xlsx');
  return sheets;
}

function parseWorksheet(worksheetLocation) {

 

  var workBook = XLSX.readFile(worksheetLocation, {
    cellDates: true
  });
  var sheet_name_list = workBook.SheetNames;
  var headers = {};
  var data = [];

  //var workSheet = workBook.Sheets['Report 1'];
  var workSheet;
  for (var key in workBook.Sheets) {
    if (workBook.Sheets.hasOwnProperty(key)) {
      workSheet = workBook.Sheets[key];
      break;
    }
  }

  for (var i in workSheet) {
    if (i[0] === '!') continue;

    //parse  the column, row, and value
    var col = i.replace(/[0-9]/g, '');
    var row = parseInt(i.replace(/[A-Z]/g, ''));
    var value = workSheet[i].v;
    //store header names
    if (row == 1) {
      headers[col] = value;
      continue;
    }
    if (!data[row]) {
      data[row] = {};
    }

    data[row][headers[col]] = value;
  }
  //droping the first two rows of empty
  data.shift();
  data.shift();
  return data;
}