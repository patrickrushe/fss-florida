/*
 * Copyright 2019 Diona.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of Diona
 * ("Confidential Information"). You shall not disclose such Confidential Information
 *  and shall use it only in accordance with the terms of the license agreement you
 *  entered into with Diona.
 */

var AppUtil = require('../../../server/framework/server/AppUtil');
var DMSecurity = AppUtil.resolve("./framework/DMSecurity");
var dbstore = AppUtil.resolve("./framework/persistence/dbstore");
var ServerUtil = AppUtil.resolve("./framework/util/ServerUtil");
var config = require('../config');
var SettingsData = require('./SettingsData');
var AuthenticationAuditLogger = AppUtil.resolve('./framework/authentication/AuthenticationAuditLogger');

var USERS_COLLECTION = "Users";



function authenticate(req, username, password, cb) {
    return dbstore.readExactlyOne_p(USERS_COLLECTION, {
        username: username
    }).then(user => {

        // Check that the user is active
        if (user.status !== "ACTIVE") {
            req.flash('error', "User account is locked. Please contact your administrator.");
            sendAuditAuthenticationEvent(req, {
                authenticated: false,
                reason: {
                    code: "ACCOUNT_LOCKED"
                }
            });
            return cb(null, false);
        } else {
            // Check the password is correct
            return DMSecurity.generateHash({
                password: password
            }).then((hash) => {
                if (user.password !== hash) {
                    // Check the number of failed login attempts
                    if (user.failedLogins && user.failedLogins >= config.failedLoginsAllowed) {
                        // Deactive the user account
                        return deactivateUserAccount(username).then(() => {
                            req.flash('error', "User account is locked. Please contact your administrator.");
                            sendAuditAuthenticationEvent(req, {
                                authenticated: false,
                                reason: {
                                    code: "ACCOUNT_LOCKED"
                                }
                            });
                            return cb(null, false);
                        });
                    } else {
                        return incrementFailedLogins(username).then(() => {
                            req.flash('error', "Invalid username or password. Please try again.");
                            sendAuditAuthenticationEvent(req, {
                                authenticated: false,
                                reason: {
                                    code: "INVALID_USERNAME_OR_PASSWORD"
                                }
                            });
                            return cb(null, false);
                        });
                    }
                } else {
                    return resetFailedLogins(username).then(() => {
                        sendAuditAuthenticationEvent(req, {
                            authenticated: true
                        });
                        return cb(null, {
                            username: user.username,
                            name: user.firstname + user.lastname,
                            role: user.role
                        });
                    });
                }
            });
        }
    }).catch(err => {
        if (err.code === "ERR_DOCUMENT_NOT_FOUND") {
            req.flash('error', "Invalid username or password. Please try again.");
            sendAuditAuthenticationEvent(req, {
                authenticated: false,
                reason: {
                    code: "INVALID_USERNAME_OR_PASSWORD"
                }
            });
            return cb(null, false);
        }
        return cb(err);
    });
}

function incrementFailedLogins(username) {
    var updateSpec = {
        $inc: {
            failedLogins: 1
        }
    };
    return dbstore.updateExactlyOne_p(USERS_COLLECTION, {
        username: username
    }, updateSpec);
}

function resetFailedLogins(username) {
    var updateSpec = {
        $set: {
            failedLogins: 0
        }
    };
    return dbstore.updateExactlyOne_p(USERS_COLLECTION, {
        username: username
    }, updateSpec);
}

function deactivateUserAccount(username) {
    var updateSpec = {
        $set: {
            status: "DEACTIVATED"
        }
    };
    return dbstore.updateExactlyOne_p(USERS_COLLECTION, {
        username: username
    }, updateSpec);
}

function serializeUser(user, cb) {
    // Takes the user object and returns the unique identifier
    return cb(null, user.username);
}

function deserializeUser(username, cb) {
    return dbstore.readExactlyOne_p(USERS_COLLECTION, {
        username: username
    }).then(user => {
        var userDtls = {
            username: user.username,
            name: user.firstname + " " + user.lastname,
            role: user.role
        };
        if (user.role == "SUPERVISOR") {
            userDtls.hasTeam = (user.supervises && user.supervises.length) ? true : false;
            userDtls.supervises = ServerUtil.ensureArray(user.supervises);
        }
        return SettingsData.getSettings().then(settings => {
            if (settings.createCaseRoles && settings.createCaseRoles.indexOf(user.role) != -1) {
                userDtls.canCreateCase = true;
            }
            return cb(null, userDtls);
        });
    });
}

// Checks to see if the user has successfully logged into the app
function verifyAuthentication(req, res, next) {
    if (req.user || req.path.startsWith('/login') || req.path == '/logout') {
        next();
    } else {
        res.render('login');
    }
}

function verifyAdminRole(req, res, next) {
    if (req.user.role === "ADMIN") {
        return next();
    } else {
        req.flash('error', "Insufficent access rights. Please contact your administrator.");
        res.render('login');
    }
}


function sendAuditAuthenticationEvent(eventRecord, eventResults) {
    var eventID = AuthenticationAuditLogger.generateEventID();
    try {
        var ip = eventRecord.headers['x-forwarded-for'] || eventRecord.connection.remoteAddress;
        console.log(ip);
        var auditEvent = {
            appID: "DMDA",
            username: eventRecord.body.username,
            ipAddress: ip,
            eventID: eventID,
            loginTimestamp: new Date(),
        };
        if (eventRecord.headers && eventRecord.headers['user-agent']) {
            auditEvent.userAgent = eventRecord.headers['user-agent'];
        }
        auditEvent.loginFailed = !eventResults.authenticated;
        if (eventResults.reason) {
            auditEvent.loginStatus = eventResults.reason.code || eventResults.reason;
        }
        AuthenticationAuditLogger.authenticationAuditLoggerEventEmitter.emit('loginAuditRequest', auditEvent);
    } catch (auditError) {
        console.log(auditError);
    }
    return eventID;
}

exports.deserializeUser = deserializeUser;
exports.serializeUser = serializeUser;
exports.authenticate = authenticate;
exports.verifyAuthentication = verifyAuthentication;
exports.verifyAdminRole = verifyAdminRole;