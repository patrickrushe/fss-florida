/*
 * Copyright 2019 Diona.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of Diona
 * ("Confidential Information"). You shall not disclose such Confidential Information
 *  and shall use it only in accordance with the terms of the license agreement you
 *  entered into with Diona.
 */

const os = require('os');
var util = require('./util');
var ClientsData = require('./ClientsData');
var AppUtil = require('../../../server/framework/server/AppUtil');
var frameworkUtil = AppUtil.resolve('./framework/util/Util');
var dbstore = AppUtil.resolve("./framework/persistence/dbstore");
var LocationService = AppUtil.resolve("./CommonServices/service/LocationService");
var CodeLookupService = AppUtil.resolve("./CommonServices/service/CodeLookupService");
var Promise = require("bluebird");

var CASES_COLLECTION = "Cases";
var CLIENTS_COLLECTION = "Clients";
var USERS_COLLECTION = "Users";

function readCase(caseID) {
    return dbstore.readAtMostOne_p(CASES_COLLECTION, {
        caseID: caseID
    });
}

function newCase(params) {
    var currentDate = frameworkUtil.toServerDateFormat(new Date());
    var caseDocument = {
        caseID: params.caseID,
        reference: params.reference,
        users: [{
            username: params.username,
            dateAssigned: currentDate
        }],
        members: []
    };

    if (params.address) {
        caseDocument.location = {
            "address": params.address
        };
    }

    if (params.customData) {
        customData = params.customData;
    }

    return dbstore.insertOne_p(CASES_COLLECTION, caseDocument);
}

function editCase(params) {
    return dbstore.readExactlyOne_p(CASES_COLLECTION, {
        caseID: params.caseID
    }).then(caseObj => {
        var currentDate = frameworkUtil.toServerDateFormat(new Date());
        var updateSpec = {
            reference: params.reference,
            username: params.username
        };

        if (params.customData) {
            updateSpec.customData = params.customData;
        }

        if (params.address) {
            updateSpec.location = {
                "address": params.address
            };
        }

        if (params.username !== caseObj.users[0].username) {
            console.log("Reassigning case " + params.caseID + " to " + params.username);
            updateSpec.users = [{
                username: params.username,
                dateAssigned: currentDate
            }];
        }

        return dbstore.updateExactlyOne_p(CASES_COLLECTION, {
            caseID: params.caseID
        }, {
            $set: updateSpec
        });
    });
}


function addOrUpdateCaseMember(params) {
    return dbstore.upsertExactlyOne_p(CASES_COLLECTION, {
        caseID: params.caseID
    }, {
        $pull: {
            members: {
                memberID: params.clientID.toString()
            }
        }
    }).then(function() {
        return dbstore.upsertExactlyOne_p(CASES_COLLECTION, {
            caseID: params.caseID
        }, {
            $push: {
                members: {
                    memberID: params.clientID.toString(),
                    role: params.role
                }
            }
        });
    });
}

function getCaseAddress(caseID, caseAddresses) {
    var addressStr = "";

    for (var i = 0; i < caseAddresses.length; i++) {
        var address = caseAddresses[i];

        if (address["Case Id"].toString() === caseID) {

            if (address["Street Number"]) {
                addressStr = address["Street Number"];
            }

            if (address["Street Name"]) {
                addressStr = addressStr + ", " + address["Street Name"];
            }

            if (address.Town) {
                addressStr = addressStr + ", " + address.Town;
            }

            if (address.County) {
                addressStr = addressStr + ", " + address.County;
            }

            if (addressStr != "") {
                if (address["State Code"]) {
                    addressStr = addressStr + ", " + address["State Code"];
                }
            }
        }
    }

    return Promise.resolve(addressStr);
}

exports.readCase = readCase;
exports.newCase = newCase;
exports.editCase = editCase;
exports.addOrUpdateCaseMember = addOrUpdateCaseMember;
exports.getCaseAddress = getCaseAddress;