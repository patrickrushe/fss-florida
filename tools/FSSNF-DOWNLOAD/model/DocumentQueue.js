"use strict";
var AppUtil = require('../../../server/framework/server/AppUtil');
var dbstore = AppUtil.resolve("./framework/persistence/dbstore");
var Util = AppUtil.resolve('./framework/util/Util');
var randomstring = require('randomstring');

var Util = AppUtil.resolve('./framework/DMPromise').Promise;

var COLLECTION_DMSOR_DOCUMENTQUEUE = "DocumentQueue";


function findAll() {
    var findAll = {};

    return dbstore.readMulti_p(COLLECTION_DMSOR_DOCUMENTQUEUE, {}, {}).then(function (documentQueue) {
        return documentQueue;
    });
}

exports.findAll = findAll;
