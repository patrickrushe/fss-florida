/*
 * Copyright 2019 Diona.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of Diona
 * ("Confidential Information"). You shall not disclose such Confidential Information
 *  and shall use it only in accordance with the terms of the license agreement you
 *  entered into with Diona.
 */

var moment = require('moment');
var util = require('./util');
var AppUtil = require('../../../server/framework/server/AppUtil');
var dbstore = AppUtil.resolve("./framework/persistence/dbstore");
var frameworkUtil = AppUtil.resolve('./framework/util/Util');
var Promise = require("bluebird");

var NOTES_COLLECTION = "Notes";
var NOTE_ID = 'Case Note Id';
var RELATED_ID = 'Case ID';
var CREATED_TIME = 'Note Creation TimeStamp';
var BEGIN_TIMESTAMP = 'Begin Timestamp';
var END_TIMESTAMP = 'End Timestamp';
var NOTE_CATEGORY = 'Note Category';
var NOTE_TYPE = 'Note Type';
var NOTE_DESCRIPTION = 'Note Description';

var VERSION = 'Verson Number';

function addNotes(noteDescriptions, noteParticipants) {
    var processedVersions = [];


    return Promise.each(noteDescriptions, function(note) {
        var previousVersion = {
            noteID: note[NOTE_ID],
            version: 0
        };

        for (var i = 0; i < processedVersions.length; i++) {
            if (processedVersions[i].noteID === noteVersion.nodeID) {
                previousVersion = processedVersions[i];
            }
        }

        if (note[VERSION] > previousVersion.version) {
            //console.log("Processing Version " + note[VERSION] + " of note " + note[
            //  RELATED_ID]);
            previousVersion.version = note[VERSION];

            var readOnly = true;

            var noteDocument = {
                noteID: note[NOTE_ID].toString(),
                relatedType: 'CASE',
                relatedID: note[RELATED_ID].toString(),
                noteType: note[NOTE_TYPE].replace(/\s/g, ''),
                username: 'caseworker',
                author: 'Case Worker',
                createdDateTime: frameworkUtil.toServerDateTimeFormat(note[CREATED_TIME])
                    .toString(),
                location: null,
                contents: note[NOTE_DESCRIPTION],
                readOnly: readOnly,
                customData: []
            };

            //noteDocument.addionalInfo = "FSFN Note Number: " + noteID: note[NOTE_ID].toString();

            noteDocument.customData.push({
                fieldID: "reference",
                value: note[NOTE_ID].toString()
            });

            noteDocument.customData.push({
                fieldID: "version",
                value: note[VERSION].toString()
            });

            if (note[BEGIN_TIMESTAMP]) {
                noteDocument.customData.push({
                    fieldID: "begin",
                    value: frameworkUtil.toServerDateTimeFormat(note[
                        BEGIN_TIMESTAMP]).toString()
                });
            }

            if (note[END_TIMESTAMP]) {
                noteDocument.customData.push({
                    fieldID: "end",
                    value: frameworkUtil.toServerDateTimeFormat(note[END_TIMESTAMP])
                        .toString()
                });
            }
            /*
            for (var j = 0; j < noteParticipants.length; j++) {
                if (noteParticipants[j]['Case ID'].toString() === note[RELATED_ID].toString() &&
                    noteParticipants[j]['Version Number'].toString() === note[VERSION].toString() &&
                    noteParticipants[j]['Case Note Id'].toString() === note[NOTE_ID].toString()
                ) {
                    noteDocument.with.push({
                        relatedID: noteParticipants[j]['Case Participant ID'].toString(),
                        relatedType: 'CLIENT'
                    });
                }
            }
            */

            return dbstore.upsertExactlyOne_p(NOTES_COLLECTION, {
                noteID: note[NOTE_ID].toString()
            }, {
                $set: noteDocument
            });
        }
        return Promise.resolve();
    });
}


function listByRelatedIDAndType(params, req) {
    return dbstore.readMulti_p(NOTES_COLLECTION, {
        relatedType: params.relatedType,
        relatedID: params.relatedID
    }, {
        _id: 0
    }, {
        createdDateTime: -1
    }).then(documents => {
        var notesList = [];
        documents.forEach(note => {
            var noteDtls = _prepareNote(note);
            notesList.push(noteDtls);
        });
        return notesList;
    });
}

function updateNoteText(noteID, newNoteText) {
    var updateSpec = {
        contents: newNoteText
    };
    return dbstore.updateExactlyOne_p(NOTES_COLLECTION, {
        noteID: noteID
    }, {
        $set: updateSpec
    });
}

function completeNote(noteID) {
    var updateSpec = {
        readOnly: true
    };
    return dbstore.updateExactlyOne_p(NOTES_COLLECTION, {
        noteID: noteID
    }, {
        $set: updateSpec
    });
}

function deleteNote(noteID) {
    return dbstore.removeExactlyOne_p(NOTES_COLLECTION, {
        noteID: noteID
    });
}

function _prepareNote(note) {
    var createdDateTimeStr = "";
    if (note.createdDateTime) {
        var createdMoment = moment(note.createdDateTime);
        createdDateTimeStr = createdMoment.format("DD/MMM/YYYY HH:mm:ss");
    }
    var noteDtls = {
        noteID: note.noteID,
        relatedID: note.relatedID,
        relatedType: note.relatedType,
        readOnly: note.readOnly,
        author: note.author,
        createdDateTime: note.createdDateTime,
        createdDateTimeStr: createdDateTimeStr,
        contents: note.contents
    };
    if (note.contents.length > 150) {
        noteDtls.contentsTrimmed = note.contents.trim().substring(0, 150).concat("...");
    }
    return noteDtls;
}

exports.addNotes = addNotes;
exports.listByRelatedIDAndType = listByRelatedIDAndType;
exports.updateNoteText = updateNoteText;
exports.completeNote = completeNote;
exports.deleteNote = deleteNote;