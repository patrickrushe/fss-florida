/*
 * Copyright 2019 Diona.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of Diona
 * ("Confidential Information"). You shall not disclose such Confidential Information
 *  and shall use it only in accordance with the terms of the license agreement you
 *  entered into with Diona.
 */

const os = require('os');
var moment = require('moment');
var util = require('./util');
var AppUtil = require('../../../server/framework/server/AppUtil');
var dbstore = AppUtil.resolve("./framework/persistence/dbstore");
var locale = AppUtil.resolve("./framework/locale/locale");
var ServerUtil = AppUtil.resolve("./framework/util/ServerUtil");
var PDFService = AppUtil.resolve("./CommonServices/service/PDFService");
var Promise = require("bluebird");
var fs = require('fs');
var builder = require('xmlbuilder');

Promise.promisifyAll(fs);

var FORM_INSTANCES_COLLECTIONS = "FormInstances";
var FORM_TYPES_COLLECTIONS = "FormTypes";
var FORM_DEFINITION_VERSIONS_COLLECTIONS = "FormDefinitionVersions";

var CASE_ID = "Case ID";
var CHILD_ID = "Child ID";
var SAFETY_PLAN_ID = "Safety Plan ID";

function readFormInstance(formID) {
    return dbstore.readAtMostOne_p(FORM_INSTANCES_COLLECTIONS, {
        formInstanceID: formID
    });
}

function addSafetyPlans(safetyPlans, safetyPlanActions) {
    return Promise.each(safetyPlans, function(safetyPlan) {

        return readFormInstance(safetyPlan[SAFETY_PLAN_ID]).then(function(existingPlan) {
            console.log(safetyPlan[SAFETY_PLAN_ID]);

            var form = {
                "formInstanceID": safetyPlan[SAFETY_PLAN_ID].toString(),
                "formTypeName": "FSSNFSafetyPlan",
                "formDefinitionName": "FSSNFSafetyPlan",
                "formTypeVersionNumber": 1,
                "username": "caseworker",
                "relatedID": safetyPlan[CASE_ID].toString(),
                "relatedType": "CASE",
                "formData": "",
                "createdDateTime": "2017-08-10T21:11:20Z",
                "binaries": [],
                "editable": false,
                "summary": "FSFN Extract of Safety Plan " + safetyPlan[SAFETY_PLAN_ID].toString()
            }

            var root = builder.create('SafetyPlan', {
                "id": "SafetyPlan"
            });

            var factorsGroup = root.ele('factors_group');

            factorsGroup.ele("case_reference", safetyPlan[CASE_ID].toString());
            factorsGroup.ele("investigation_id", "12345677");

            form.formData = root;

            return dbstore.upsertExactlyOne_p(FORM_INSTANCES_COLLECTIONS, {
                formInstanceID: safetyPlan[SAFETY_PLAN_ID].toString()
            }, {
                $set: form
            });
        });
    });
}

function listByRelatedIDAndType(params) {
    return dbstore.readMulti_p(FORM_INSTANCES_COLLECTIONS, {
        "relatedType": params.relatedType,
        "relatedID": params.relatedID
    }, {
        _id: 0,
        formData: 0
    }, {
        createdDateTime: -1
    }).then(documents => {
        var formInstances = [];
        return Promise.each(documents, formInstance => {
            formInstance.createdDateTimeStr = "";
            if (formInstance.createdDateTime) {
                var createdMoment = moment(formInstance.createdDateTime);
                formInstance.createdDateTimeStr = createdMoment.format("DD/MMM/YYYY HH:mm:ss");
            }
            return dbstore.readExactlyOne_p(FORM_TYPES_COLLECTIONS, {
                name: formInstance.formTypeName
            }).then(formType => {
                formInstance.localisedName = formType.locales[''];
                return dbstore.readExactlyOne_p(FORM_DEFINITION_VERSIONS_COLLECTIONS, {
                    formDefinitionName: formInstance.formDefinitionName,
                    formDefinitionVersionNumber: formInstance.formTypeVersionNumber
                }, {
                    pdfFormBinaryID: 1
                });
            }).then(formDefinitionVersion => {
                formInstance.pdfAvailable = (formDefinitionVersion.pdfFormBinaryID) ? true : false;
                formInstances.push(formInstance);
            });
        }).then(() => {
            return formInstances;
        });
    });
}

function createPDF(formInstanceID) {
    return dbstore.readExactlyOne_p(FORM_INSTANCES_COLLECTIONS, {
        formInstanceID: formInstanceID
    }).then((formInstance) => {
        var binaries = ServerUtil.ensureArray(formInstance.binaries);
        var _files = (binaries.length > 0) ? {} : null;
        return Promise.each(binaries, (binaryDetails, index) => {
            return util.getBinaryFile(binaryDetails.binaryID).then(base64Str => {
                var decodedData = new Buffer(base64Str, 'base64');
                var tmpFilePath = os.tmpdir() + "/" + new Date().getTime();
                return fs.writeFileAsync(tmpFilePath, decodedData).then(() => {
                    _files["file" + index] = {
                        name: binaryDetails.name,
                        path: tmpFilePath
                    };
                });
            });
        }).then(() => {
            var pdfParams = {
                tmpPath: os.tmpdir(),
                formData: formInstance.formData,
                formDefinitionName: formInstance.formDefinitionName,
                formDefinitionVersionNumber: formInstance.formTypeVersionNumber
            };
            if (_files) {
                pdfParams._files = _files;
            }
            return PDFService.createPDFFromFormDefinition(pdfParams);
        });
    });
}

exports.listByRelatedIDAndType = listByRelatedIDAndType;
exports.createPDF = createPDF;
exports.addSafetyPlans = addSafetyPlans;