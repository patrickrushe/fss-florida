/*
 * Copyright 2019 Diona.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of Diona
 * ("Confidential Information"). You shall not disclose such Confidential Information
 *  and shall use it only in accordance with the terms of the license agreement you
 *  entered into with Diona.
 */

const os = require('os');
var AppUtil = require('../../../server/framework/server/AppUtil');
var BinaryGridFS = AppUtil.resolve("./CommonServices/datastore/BinaryGridFS");
var Promise = require("bluebird");
var fs = require('fs');
Promise.promisifyAll(fs);

/**
 * Calculates the age in years based on a birth date.
 *
 * @param {Date}
 *        birthDate The birth date to calculate the age for.
 * @param {Date}
 *        calculationDate Optional, the date to use for the calculation, if not set the current date is used. This can
 *        be used to calculate an age at a particular point in time, for example age at death.
 * @return {Number} The age in years.
 */
function calculateAge(birthDate, calculationDate) {

    if (!birthDate) {
        return 0;
    }

    // Determine the date to use for the calculation
    var calculationMs = (calculationDate) ? calculationDate.getTime() : Date.now();

    // Calculate the number of milliseconds since the birth date
    var ageDifMs = calculationMs - birthDate.getTime();

    // Check that the birth Date is before the calculation date.
    if (ageDifMs < 0) {
        return 0;
    }

    // Convert the milliseconds into a date
    var ageDate = new Date(ageDifMs);

    /*
     * Subtract 1970 (the year used for millisecond calculations in java script) from the age date and convert to a
     * positive number.
     */
    return Math.abs(ageDate.getUTCFullYear() - 1970);
}

function getBinaryFile(binaryID) {
    return BinaryGridFS.readByBinaryID(binaryID).then(binaryData => {
        return binaryData.data.toString();
    });
}

function getExtensionFromAttachmentType(type) {
    return "." + type.substring(type.indexOf('/') + 1, type.length);
}

function downloadBinaryFile(req, res, binaryID, filename) {
    return getBinaryFile(binaryID).then(base64Str => {
        var decodedData = new Buffer(base64Str, 'base64');
        return downloadFile(req, res, decodedData, filename);
    });
}

function downloadFile(req, res, data, filename) {
    var tmpFilePath = os.tmpdir() + "/" + new Date().getTime();
    return fs.writeFileAsync(tmpFilePath, data).then(() => {
        return res.download(tmpFilePath, filename, function (err) {
            if (err) {
                _deleteFileSilently(tmpFilePath);
                throw err;
            } else {
                _deleteFileSilently(tmpFilePath);
            }
        });
    });
}

function viewPDF(req, res, data, filename) {
    res.setHeader('Content-disposition', 'inline; filename=' + filename);
    res.setHeader('content-type', 'application/pdf');
    return res.send(data);
}

function _deleteFileSilently(path) {
    return fs.unlinkAsync(path).catch(function (e) {
        // If the file doesn't exist, ignore the error
        if (e.cause.code !== 'ENOENT') {
            throw e;
        }
    });
}

exports.calculateAge = calculateAge;
exports.getBinaryFile = getBinaryFile;
exports.getExtensionFromAttachmentType = getExtensionFromAttachmentType;
exports.downloadBinaryFile = downloadBinaryFile;
exports.downloadFile = downloadFile;
exports.viewPDF = viewPDF;