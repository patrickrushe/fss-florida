/*
 * Copyright 2019 Diona.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of Diona
 * ("Confidential Information"). You shall not disclose such Confidential Information
 *  and shall use it only in accordance with the terms of the license agreement you
 *  entered into with Diona.
 */

// The date format used by the application, valid values are 'dd/mm/yyyy' or 'mm/dd/yyyy'
exports.date_format = "dd/mm/yyyy";

// The max age in milliseconds of the session cookie
exports.cookieMaxAge = 30 * 60 * 1000;

// The number of days that users are warned before their password expires.
exports.userPasswordExpiryWarning = 7;